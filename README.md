# php-bibtex

[![pipeline status](https://gitlab.cylab.be/cylab/php-bibtex/badges/master/pipeline.svg)](https://gitlab.cylab.be/cylab/php-bibtex/-/commits/master)
[![coverage report](https://gitlab.cylab.be/cylab/php-bibtex/badges/master/coverage.svg)](https://gitlab.cylab.be/cylab/php-bibtex/-/commits/master)

A small PHP lirary for writing bibtex programatically.

## Installation

```bash
composer require cylab/php-bibtex
```

## Usage

```php
require "vendor/autoload.php";

use Cylab\Bibtex\Article;

$a = new Article();
$a->setTitle("The title of the work")
    ->setAuthor("Thibault Debatty")
    ->setJournal("The name of the journal")
    ->setYear(1993)
    ->setMonth(7)
    ->setVolume(4)
    ->setNote("An optional note")
    ->setNumber(2)
    ->setPages("201-213");

echo $a;
```

Will produce:

```
@article{thibault-debatty-1993-i6eVEd,
  author = {Thibault Debatty},
  title = {The title of the work},
  journal = {The name of the journal},
  year = 1993,
  volume = 4,
  number = 2,
  pages = {201-213},
  month = 7,
  note = {An optional note}
}
```
