<?php

namespace Cylab\Bibtex;

/**
 * An article in a conference proceedings.
 */
class PhdThesis extends Entry
{
    public function __construct()
    {
        parent::setType("phdthesis");
    }

    protected function required() : array
    {
        return ["title", "school", "year"];
    }

    protected function optional() : array
    {
        return ["type", "address", "month", "note"];
    }
}
