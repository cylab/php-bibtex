<?php

namespace Cylab\Bibtex;

/**
 * A part of a book with its own title.
 *
 * @author tibo
 */
class InCollection extends Entry
{

    public function __construct()
    {
        parent::setType("incollection");
    }

    protected function optional(): array
    {
        return ["editor", "pages", "organization", "publisher", "address", "month"];
    }

    protected function required(): array
    {
        return ["title", "booktitle", "year"];
    }
}
