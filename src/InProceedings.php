<?php

namespace Cylab\Bibtex;

/**
 * An article in a conference proceedings.
 */
class InProceedings extends Entry
{
    public function __construct()
    {
        parent::setType("inproceedings");
    }

    protected function required() : array
    {
        return ["title", "booktitle", "year"];
    }

    protected function optional() : array
    {
        return ["editor", "number", "series", "pages", "address",
            "month", "organization", "publisher", "note"];
    }
}
