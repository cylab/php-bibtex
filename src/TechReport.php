<?php

namespace Cylab\Bibtex;

/**
 * TechReport.
 *
 * https://www.bibtex.com/t/template-techreport/
 */
class TechReport extends Entry
{
    public function __construct()
    {
        parent::setType("techreport");
    }

    protected function required() : array
    {
        return ["title", "year", "institution"];
    }

    protected function optional() : array
    {
        return ["type", "number", "month", "doi", "note"];
    }
}
