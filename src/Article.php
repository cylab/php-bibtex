<?php

namespace Cylab\Bibtex;

/**
 * An article from a journal or magazine.
 */
class Article extends Entry
{
    public function __construct()
    {
        parent::setType("article");
    }

    protected function required() : array
    {
        return ["title", "journal", "year", "volume"];
    }

    protected function optional() : array
    {
        return ["number", "pages", "month", "doi", "note", "key"];
    }
}
