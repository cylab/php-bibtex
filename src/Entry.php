<?php

namespace Cylab\Bibtex;

abstract class Entry
{
    private $type;
    private $fields = [];
    private $authors = [];
    private $keywords = [];

    protected function setType(string $type)
    {
        $this->type = $type;
    }

    public function slug(string $string) : string
    {
        return strtolower(str_replace(" ", "-", $string));
    }

    public function key() : string
    {
        $this->validate();

        return $this->slug($this->authors[0]) . "-".
            $this->fields["year"] . "-" .
            substr(base64_encode(hex2bin(md5($this->fields["title"]))), 0, 6);
    }

    abstract protected function required() : array;
    abstract protected function optional() : array;

    public function addAuthor(string $author)
    {
        $this->authors[] = $author;
        return $this;
    }

    public function addKeyword(string $keyword)
    {
        $this->keywords[] = $keyword;
        return $this;
    }

    public function setTitle(string $title)
    {
        $this->fields["title"] = $title;
        return $this;
    }

    public function setJournal(string $journal)
    {
        $this->fields["journal"] = $journal;
        return $this;
    }

    public function setYear(int $year)
    {
        $this->fields["year"] = $year;
        return $this;
    }

    public function setVolume(int $volume)
    {
        $this->fields["volume"] = $volume;
        return $this;
    }

    public function setNumber(int $number)
    {
        $this->fields["number"] = $number;
        return $this;
    }

    public function setPages(string $pages)
    {
        $this->fields["pages"] = $pages;
        return $this;
    }

    public function setMonth(int $month)
    {
        $this->fields["month"] = $month;
        return $this;
    }

    public function setDOI(string $doi)
    {
        $this->fields["doi"] = $doi;
        return $this;
    }

    public function setNote(string $note)
    {
        $this->fields["note"] = $note;
        return $this;
    }

    public function setSchool(string $school)
    {
        $this->fields["school"] = $school;
        return $this;
    }

    public function setBooktitle(string $booktitle)
    {
        $this->fields["booktitle"] = $booktitle;
        return $this;
    }

    public function setUrl(string $url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new \Exception("Not a valid URL: " . $url);
        }

        $this->fields["url"] = $url;
    }

    public function setPublisher(string $publisher)
    {
        $this->fields["publisher"] = $publisher;
        return $this;
    }

    public function setAddress(string $address)
    {
        $this->fields["address"] = $address;
        return $this;
    }

    public function setInstitution(string $institution)
    {
        $this->fields["institution"] = $institution;
        return $this;
    }

    private function formatField($field)
    {
        $value = $this->fields[$field];
        if (is_integer($value)) {
            return "  $field = $value,\n";
        }

        return "  $field = {" . $value . "},\n";
    }

    public function toString()
    {
        $this->validate();

        $s = '@' . $this->type . '{' . $this->key() . ",\n";
        $s .= "  author = {" . implode(" and ", $this->authors) . "},\n";
        $s .= "  keywords = {" . implode(", ", $this->keywords) . "},\n";

        foreach ($this->required() as $field) {
            $s .= $this->formatField($field);
        }

        $optionals = $this->optional();
        $optionals[] = "url";

        foreach ($optionals as $field) {
            if (isset($this->fields[$field])) {
                $s .= $this->formatField($field);
            }
        }

        // remove trailing ','
        $s = trim($s, ", \t\n\r\0\x0B");
        $s .= "\n}\n";

        return $s;
    }

    public function validate() : void
    {
        foreach ($this->required() as $field) {
            if (! isset($this->fields[$field])) {
                throw new \Exception("missing field $field!");
            }
        }
    }

    public function __toString()
    {
        return $this->toString();
    }
}
