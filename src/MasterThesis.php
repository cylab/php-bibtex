<?php

namespace Cylab\Bibtex;

/**
 * An article in a conference proceedings.
 */
class MasterThesis extends Entry
{
    public function __construct()
    {
        parent::setType("masterthesis");
    }

    protected function required() : array
    {
        return ["title", "school", "year"];
    }

    protected function optional() : array
    {
        return ["type", "address", "month", "note"];
    }
}
