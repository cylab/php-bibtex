<?php

namespace Cylab\Bibtex;

class EntryTest extends \PHPUnit\Framework\TestCase
{
    public function testArticle()
    {
        $a = new Article();
        $a->setTitle("The title of the work")
            ->addAuthor("Thibault Debatty")
            ->addKeyword("RMA-CISS-RUCD")
            ->addKeyword("RMA-CISS-XYZ")
            ->setJournal("The name of the journal")
            ->setYear(1993)
            ->setMonth(7)
            ->setVolume(4)
            ->setNote("An optional note")
            ->setNumber(2)
            ->setPages("201-213");

        $this->assertEquals(
            file_get_contents(__DIR__ . "/article.bib"),
            $a->toString()
        );
    }

    public function testReport()
    {
        $r = new TechReport();
        $r->setTitle("A nice poster")
            ->setInstitution("Cyber Defence Lab")
            ->setYear(2023)
            ->addAuthor("Thibault Debatty");

        $this->assertEquals(
            file_get_contents(__DIR__ . "/techreport.bib"),
            $r->toString()
        );
    }

    /**
     * @group exceptions
     */
    public function testMissingRequiredFieldException()
    {
        $this->expectException(\Exception::class);
        $a = new InCollection();
        $a->addAuthor("Thibault Debatty")
                ->setTitle("Some book chapter");
        echo $a->toString();
    }
}
